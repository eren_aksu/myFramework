@userServiceTest      @apitest
Feature: User Service Tests

  Scenario:Getting the user without authenticated
    Given Send get request for "Ectogenesis" username to get user info without sending bearer token
    Then See there is no user info in the response

  Scenario: Getting user with bearerToken
    Given Send get request for "Ectogenesis" username to get user info with bearerToken
    Then See "Ectogenesis" username info can be seen in the response

  Scenario: Getting user email address
    Given Send get request to see user email
    Then See user email is "eren.aksu1632@gmail.com"

  Scenario:Adding new user email adresses
    Given Send post request to add new emails
    Then See new emails are added to user
    #This scenario will be failed as added emails need to be validated first


  Scenario Outline: Check if repo is created without mandatory parameter
    Given Send post request to create repo by only using optional "<params>"
    Then See repo is not created without name parameter

    Examples:
    |params|
    |auto_init|
    |description|
    |gitignore_template|
    |has_downloads|
    |has_issues|
    |has_wiki|
    |homepage|
    |private|
    |team_id|




