package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import restassured.tests.UserService;

public class UserServiceStepDef {

    UserService userService;

    @Given("^Send get request for \"([^\"]*)\" username to get user info without sending bearer token$")
    public void sendGetRequestForUsernameToGetUserInfoWithoutSendingBearerToken(String arg0) {
        userService.sendGetRequestWithoutToken();
    }

    @Then("^See there is no user info in the response$")
    public void seeThereIsNoUserInfoInTheResponse() {
        userService.assertUserInfoNotExist();
    }


    @Given("^Send get request for \"([^\"]*)\" username to get user info with bearerToken$")
    public void sendGetRequestForUsernameToGetUserInfoWithBearerToken(String arg0) {
        userService.sendGetRequestWithBearerToken();
    }

    @Then("^See \"([^\"]*)\" username info can be seen in the response$")
    public void seeUsernameInfoCanBeSeenInTheResponse(String arg0)  {
        userService.assertUserExists(arg0);
    }


    @Given("^Send get request to see user email$")
    public void sendGetRequestToSeeUserEmail() {
        userService.sendGetRequestToUserEmailEndPoint();
    }

    @Then("^See user email is \"([^\"]*)\"$")
    public void seeUserEmailIs(String eMail){
        userService.assertUserEmailIsCorrect(eMail);
    }

    @Given("^Send post request to add new emails$")
    public void sendPostRequestToAddNewEmails() {
        userService.sendPostRequestToAddEmail();
    }

    @Then("^See new emails are added to user$")
    public void seeNewEmailsAreAddedToUser() {
        userService.assertMailsAreAdded();
    }

    @Given("^Send post request to create repo by only using optional \"([^\"]*)\"$")
    public void sendPostRequestToCreateRepoByOnlyUsingOptional(String param) {
        userService.sendPostRequestWithoutMandatoryParamsToCreateRepo(param);
    }

    @Then("^See repo is not created without name parameter$")
    public void seeRepoIsNotCreatedWithoutNameParameter() {
        userService.assertRepoIsNotCreated();
    }


}
