package restassured.tests;

import base.BasePage;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Title;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;


public class UserService extends BasePage {
    private String bearerToken = "7b1cf7f83dc19fc5305a2c25fc021c57d2858503";
    private String baseUrl = "https://api.github.com";
    private String userBasePath = "/user";
    private String userEmailBasePath = "/user/emails";
    private String userRepoBasePath = "/user/repos";


    Response response;

    @Title("User Service Test")
    @Test
    public void sendGetRequestWithoutToken() {
        response = SerenityRest.given()
                .header("accept", "application/vnd.github.v3+json")
                .contentType(ContentType.JSON)
                .get(baseUrl + userBasePath);


    }

    @Title("User Service Test")
    @Test
    public void assertUserInfoNotExist() {
        response.then().body("message", Matchers.equalTo("Requires authentication"));
    }

    @Title("User Service Test")
    @Test
    public void sendGetRequestWithBearerToken() {
        response = SerenityRest.given()
                .auth()
                .oauth2(bearerToken)
                .header("accept", "application/vnd.github.v3+json")
                .contentType(ContentType.JSON)
                .get(baseUrl + userBasePath);
    }

    @Title("User Service Test")
    @Test
    public void assertUserExists(String userName) {
        response.then().statusCode(200);
        response.then().body("login", Matchers.equalTo(userName));
    }

    @Title("User Service Test")
    @Test
    public void sendGetRequestToUserEmailEndPoint() {
        response = SerenityRest.given()
                .auth()
                .oauth2(bearerToken)
                .header("accept", "application/vnd.github.v3+json")
                .contentType(ContentType.JSON)
                .get(baseUrl + userEmailBasePath);
    }

    @Title("User Service Test")
    @Test
    public void assertUserEmailIsCorrect(String userEmail) {
        response.then().statusCode(200);
        String responseBody = response.asString();
        Assert.assertTrue(responseBody.contains(userEmail));
    }

    @Title("User Service Test")
    @Test
    public void sendPostRequestToAddEmail() {
        File addEmailRequest = new File("src/test/resources/testdata/AddEmailRequest.json");
        response = SerenityRest.given()
                .auth()
                .oauth2(bearerToken)
                .header("accept", "application/vnd.github.v3+json")
                .contentType(ContentType.JSON)
                .body(addEmailRequest)
                .post(baseUrl + userEmailBasePath);
    }

    @Title("User Service Test")
    @Test
    public void assertMailsAreAdded() {
        response.then().statusCode(200);
        System.out.println(response.asString());
    }

    @Title("User Service Test")
    @Test
    public void sendPostRequestWithoutMandatoryParamsToCreateRepo(String param) {
        switch (param) {
            case "auto_init":
                File createRepoAuthInit = new File("src/test/resources/testdata/CreateRepoWithAuthInit.json");
                response = SerenityRest.given()
                        .auth()
                        .oauth2(bearerToken)
                        .header("accept", "application/vnd.github.v3+json")
                        .contentType(ContentType.JSON)
                        .body(createRepoAuthInit).when().log().all()
                        .post(baseUrl + userRepoBasePath);
                break;
            case "description":
                File createRepoDescription = new File("src/test/resources/testdata/CreateRepoWithDescription.json");
                response = SerenityRest.given()
                        .auth()
                        .oauth2(bearerToken)
                        .header("accept", "application/vnd.github.v3+json")
                        .contentType(ContentType.JSON)
                        .body(createRepoDescription).when().log().all()
                        .post(baseUrl + userRepoBasePath);
                break;
            case "gitignore_template":
                File createRepoGitIgnoreTemplate = new File("src/test/resources/testdata/CreateRepoWithGitIgnoreTemplate.json");
                response = SerenityRest.given()
                        .auth()
                        .oauth2(bearerToken)
                        .header("accept", "application/vnd.github.v3+json")
                        .contentType(ContentType.JSON)
                        .body(createRepoGitIgnoreTemplate).when().log().all()
                        .post(baseUrl + userRepoBasePath);
                break;
            case "has_downloads":
                File createRepoHasDownloads = new File("src/test/resources/testdata/CreateRepoWithHasDownloads.json");
                response = SerenityRest.given()
                        .auth()
                        .oauth2(bearerToken)
                        .header("accept", "application/vnd.github.v3+json")
                        .contentType(ContentType.JSON)
                        .body(createRepoHasDownloads).when().log().all()
                        .post(baseUrl + userRepoBasePath);
                break;

            case "has_issues":
                File createRepoHasIssues = new File("src/test/resources/testdata/CreateRepoWithHasIssues.json");
                response = SerenityRest.given()
                        .auth()
                        .oauth2(bearerToken)
                        .header("accept", "application/vnd.github.v3+json")
                        .contentType(ContentType.JSON)
                        .body(createRepoHasIssues).when().log().all()
                        .post(baseUrl + userRepoBasePath);
                break;
            case "has_wiki":
                File createRepoHasWiki = new File("src/test/resources/testdata/CreateRepoWithHasWiki.json");
                response = SerenityRest.given()
                        .auth()
                        .oauth2(bearerToken)
                        .header("accept", "application/vnd.github.v3+json")
                        .contentType(ContentType.JSON)
                        .body(createRepoHasWiki).when().log().all()
                        .post(baseUrl + userRepoBasePath);
                break;

            case "homepage":
                File createRepoHomePage = new File("src/test/resources/testdata/CreateRepoWithHomePage.json");
                response = SerenityRest.given()
                        .auth()
                        .oauth2(bearerToken)
                        .header("accept", "application/vnd.github.v3+json")
                        .contentType(ContentType.JSON)
                        .body(createRepoHomePage).when().log().all()
                        .post(baseUrl + userRepoBasePath);
                break;
            case "private":
                File createRepoPrivate = new File("src/test/resources/testdata/CreateRepoWithPrivate.json");
                response = SerenityRest.given()
                        .auth()
                        .oauth2(bearerToken)
                        .header("accept", "application/vnd.github.v3+json")
                        .contentType(ContentType.JSON)
                        .body(createRepoPrivate).when().log().all()
                        .post(baseUrl + userRepoBasePath);
                break;
            case "team_id":
                File createRepoTeamId = new File("src/test/resources/testdata/CreateRepoWithTeamId.json");
                response = SerenityRest.given()
                        .auth()
                        .oauth2(bearerToken)
                        .header("accept", "application/vnd.github.v3+json")
                        .contentType(ContentType.JSON)
                        .body(createRepoTeamId).when().log().all()
                        .post(baseUrl + userRepoBasePath);
                break;



        }
    }


    @Title("User Service Test")
    @Test
    public void assertRepoIsNotCreated() {
        response.then().statusCode(422);
        response.then().body("message", Matchers.equalTo("Repository creation failed."));
    }


}
