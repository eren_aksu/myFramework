--Run,Install,Details--

1-Project could be pulled from https://gitlab.com/eren_aksu/myFramework

2-Running it through the cucumber jvm is not generating reports, you can run with maven verify and reports can be seen.

3-In this project; feature files, java classes and step definitions are created only for user service.
I would do the same for other services to avoid mess. Please do the same if you would like to add some test cases.

4-In case of writing tests for ui, chromedriver.exe has been added.
